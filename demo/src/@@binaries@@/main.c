/*
** main.c for  in /home/wapiflapi/Projects/wproject
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Fri Apr 27 11:09:48 2012 Wannes Rombouts
** Last update Fri Apr 27 11:10:31 2012 Wannes Rombouts
*/

#include	<stdio.h>

int		main(int argc, char **argv)
{
  (void) argc, (void) argv;
  printf("Hello world from @@binaries@@\n");
  return 0;
}
