#!/usr/bin/env python2
## wproject.py for  in /home/wapiflapi/Projects/wproject
##
## Made by Wannes Rombouts
## Login   <rombou_w@epitech.net>
##
## Started on  Fri Apr 27 08:57:33 2012 Wannes Rombouts
## Last update Fri Apr 27 19:02:37 2012 Wannes Rombouts
##

import argparse
import ConfigParser
import shutil
import os
import re


def delFile(fpath) :
    if os.path.isdir(fpath) :
        shutil.rmtree(fpath)
    else :
        os.remove(fpath)


def genPart(data, part) :

    parts = [part]
    datas = [data.copy()]

    for m in re.findall('(@@(.*?)@@)', part) :
        if m[1] not in data :
            print "Warning : ignoring expand [%s]" % m[0]
            continue

        newdatas = []
        newparts = []
        for p, d  in zip(parts, datas) :
            for v in data[m[1]] :
                newparts.append(p.replace(m[0], v))
                newdatas.append(d.copy())
                newdatas[-1][m[1]] = [v]
        parts = newparts
        datas = newdatas

    return parts, datas


def genFileContent(data, path) :

    f = open(path)

    buffering = False
    generated = ""
    buf = ""

    for l in f :
        if l.startswith("#@") :
            buffering ^= 1
        else :
            buf += l

        if not buffering :
            generated += "".join(genPart(data, buf)[0])

    if buf :
        generated += "".join(genPart(data, buf)[0])

    return generated


def genFile(data, fpath, npath) :

    names, ndatas = genPart(data, npath)
    for name, ndata in zip(names, ndatas) :

        print "\tMaking file [%s]" % name

        generated = genFileContent(ndata, fpath)

        with open(name, 'w+') as f :
            f.write(generated)


def genDir(data, dirpath, filenames) :

    names, ndatas = genPart(data, dirpath)
    for name, ndata in zip(names, ndatas) :

        print "\tMaking dir [%s]" % name

        if name != dirpath :
            os.mkdir(name)

        for fname in filenames :
            fpath = os.path.join(dirpath, fname)

            print "Looking at file [%s]" % fpath
            genFile(ndata, fpath, os.path.join(name, fname))

    if dirpath not in names :
        delFile(dirpath)


def filterIgnore(ignore, names) :

    remove = []

    for n in names :
        for i in ignore :
            if re.match(i, n) :
                remove.append(n)
                break

    for r in remove :
        names.remove(r)


if __name__ == "__main__" :

    # parse program's options

    parser = argparse.ArgumentParser(description='Simple and personal project creator.')
    parser.add_argument('name',
                        help='Project name.')
    parser.add_argument('-t', '--trust', action='store_true',
                        help='Dont atempt to parse template\'s option file and trust user.')
    parser.add_argument('-l', '--location', default='.',
                        help='Project location.')
    parser.add_argument('-p', '--path', default=os.path.expanduser('~/.config/wproject/'),
                        help='Project generation files.')
    parser.add_argument('args', nargs='*',
                        help='Template arguments.')


    opts = parser.parse_args()
    opts.location = os.path.join(opts.location, opts.name)

    # load tempalte's config

    print opts.args
    if not opts.trust :
        config = ConfigParser.ConfigParser()
        config.read(os.path.join(opts.path, 'wproject.cfg'))
        description = config.get('general', 'description')
        topts = config.options('options')
    else :
        description = 'You should know what to do ... I can\'t help.'
        topts = [x for x in opts.args if x.startswith('++')]

    # parse template's options

    parser = argparse.ArgumentParser(description=description, prefix_chars="+")
    for opt in topts :
        if not opts.trust :
            optv = config.get('options', opt)
            try :
                opth = config.get('help', opt)
            except :
                opth = None
        else :
            optv = '*'
            opth = None

        parser.add_argument('++%s' % opt,
                            nargs=optv == '?' and 1 or optv,
                            required=optv != '*' and optv != '?',
                            help=opth,
                            default=[])
    topts = parser.parse_args(opts.args)

    data = dict(isinstance(v, (list, tuple)) and (o, v) or (o, [v])
                for o, v in topts.__dict__.iteritems())

    # copy the template (we dont touch the original in any case)

    try :
        shutil.copytree(opts.path, opts.location)
    except OSError as e :
        print "Ooops ! %s" % e
        exit(1)

    for dirpath, dirnames, filenames in os.walk(opts.location) :
        if ".wignore" not in filenames :
            ignore = []
        else :
            with open(os.path.join(dirpath, ".wignore")) as f :
                ignore = [l.strip() for l in f.readlines() if l[0] != '#']

        print "Looking at dir [%s]" % dirpath

        filterIgnore(ignore, dirnames)
        filterIgnore(ignore, filenames)

        genDir(data, dirpath, filenames)
